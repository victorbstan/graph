require 'pry'

module Transport
  # destination must respond to "m"
  def deliver(payload, destination, exclusion)
    raise "Payload should be a Message object" unless payload.is_a? Message
    @ex = nil
    if destination.is_a? Array
      @ex = [exclusion].flatten
      destination.each do |d|
        d.m(payload) unless @ex.include? d
      end
    else
      @ex = [exclusion].flatten
      destination.m(payload) unless @ex.include? destination
    end

    puts "#{self} is delivering: #{payload} to #{destination}, but not to: #{@ex}"
  end
end

class Graph
  # branches can have two 'ends'
  # each end is attached to one node
  # each node can have many branches

  attr_reader :total_nodes, :total_branches,
              :branches, :nodes, :branch, :node

  def initialize
    @total_branches = 0
    @total_nodes = 0
    @branches = []
    @nodes = []
  end

  # make a new branch
  def new_b
    @total_branches += 1

    # requirements for a new branch, new branch
    id = @total_branches
    b = Branch.new id, new_n, new_n

    # track branches
    @branches << b

    return b
  end

  # make a new node
  def new_n(m=nil)
    @total_nodes += 1

    # requirements for a new node, new node
    id = @total_nodes
    n = Node.new id

    # track nodes
    @nodes << n

    return n
  end

  def new_snake_b
    @total_branches += 1

    # requirements for a new branch, new branch
    id = @total_branches
    last_n = self.nodes.last || new_n
    b = Branch.new id, last_n, new_n

    # track branches
    @branches << b

    return b
  end

  def branch(id)
    @branches.map {|b| return b if b.id == id}
  end

  def node(id)
    @nodes.map {|n| return n if n.id == id}
  end

  def gen1(itter)
    itter.times do |i|
      r = self.new_b
    end
  end

  def gen2(itter)
    itter.times do |i|
      r = self.new_snake_b
    end
  end
end

class Branch
  include Transport

  attr_accessor :id, :n1, :n2, :m

  def initialize(id, n1, n2)
    @id = id
    @n1 = n1
    @n2 = n2

    # add branch to nodes
    @n1.bs self
    @n2.bs self
  end

  def m(message=nil)
    if message
      @m = message
      forward_m
    end
    @m
  end

  def to_s
    "#{self.class.name} #{@id}"
  end

private

  def forward_m
    # payload, destination, exclusion
    @m.sender = self
    deliver(@m, @n2, [self]) unless @m.nil?
  end

  # def send_backwards_m
  #   # payload, destination, exclusion
  #   @m.sender = self
  #   deliver(@m, @n1, self) unless @m.nil?
  # end
end

class Node
  include Transport

  attr_accessor :id, :m, :bs

  def initialize(id)
    @id = id
    @bs = []
  end

  # collect branches
  def bs(b=nil)
    @bs << b unless (@bs.include? b || b.nil?)
    @bs
  end

  def m(message=nil)
    @m ||= message
    if @m
      @m.sender = self
      # payload, destination, exclusion
      deliver(@m, @bs, [self, @bs[0]]) # acts as forward
    end
    @m
  end

  def to_s
    "#{self.class.name} #{@id}"
  end
end

class Message
  attr_accessor :text, :author, :sender
  def initialize(text, author, sender)
    @text = text
    @author = author
    @sender = sender
  end

  def to_s
    "#{self.class.name} #{@text}"
  end
end

# WORKSHEET #########################################################

# GRAPH

puts g = Graph.new

puts "\n\n Generate 5 branches\n\n"
# g.gen1 100
g.gen2 100

# (n1)[b1](n2)-(n2)[b2](n3)-(n3)[b3](n4)
# (n1)[b1](n2)|(n3)[b2](n4)

######################################################################
# save the best for last, to instantiate a REPL with the loaded script
binding.pry
